var config = require('./config')
var express = require('express');
var static = require('node-static');
var router = express.Router();
var app = express();

var packageFile = require('../package.json'); //to get version data
var airlinesRoute = require('./api/airlines');
var airportsRoute = require('./api/airports');
var searchRoute = require('./api/search');




/**
 * Express Router
 */
router.get('/airlines',airlinesRoute.get);
router.get('/airports',airportsRoute.get);
router.get('/search',searchRoute.get);


router.use(express.static(__dirname + '/../public'));



app.use(router);

app.listen(3000, function () {
    console.log('\n-----------------------');
    console.log(packageFile.name+' '+packageFile.version+' is ready\n');
    console.log(' http://localhost:3000/ \n\n');
});

