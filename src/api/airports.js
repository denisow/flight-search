var providedAPIService = require('../provided-api-service');

/**
 * /airports GET  Provides an airport search.
 * Example request: airports?q=Melbourne
 * @param  (required) - text based search param
 *
 */
exports.get = function(req,res){
    if(!req.query.q || req.query.q.length < 2){
        res.json({'error':'Query must contain at least two letters','param': 'q'});
        return;
    }
    providedAPIService.get('/airports?q='+req.query.q,res)
};

