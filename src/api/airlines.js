var providedAPIService = require('../provided-api-service');


/**
 * /airlines  Provides a list of airlines. Takes no parameters.
 */
exports.get = function(req,res){
    providedAPIService.get('/airlines',res)
};