var providedAPIService = require('../provided-api-service');
var moment = require('moment');
var config = require('../config')

/**
 * Provides a list of available flights for all available airline.
 *
 * @param  date         (required) base day, format YYYY-MM-DD
 * @param  from         (required) IATA code of departure airport
 * @param  to           (required) IATA code of arrival airport
 * @param  comingDays   (required) int +/- comingDays before and after base day
 * @param  res          (required) express response
 * @return              Array of days (consisted of date and list of flights)
 */
var search = function(date, from, to, comingDays, res){
    var output = [];
    var flightSearchCount = 0;
    //get list of airlines
    providedAPIService.get('/airlines',res,function(airlines){
        //make list of days
        if(comingDays == 0){
            output.push({'date':date,'flights':[]});
        }else{
            for (var i = parseInt(comingDays); i >= -1*parseInt(comingDays); i--) {
                if(i == 0){
                    continue; //exclude current day, in case comingDays search
                }
                var flightDate = moment(date).subtract(i, 'day').format(config.dateFormat);
                output.push({'date':flightDate,'flights':[]});
            }
        }
        output.forEach(function(dateTab){
            airlines.forEach(function(company){
                flightSearchCount++;
                //get flights in each company each day
                providedAPIService.get('/flight_search/'+company.code+'?date='+dateTab.date+'&from='+from+'&to='+to,res,function(data){
                    if(Array.isArray(data)){
                        dateTab.flights = dateTab.flights.concat(data);
                    }
                    // return when last flight list was loaded
                    flightSearchCount--;
                    if(flightSearchCount === 0){
                        res.json(output);
                    }
                });
            });
        });
    })
}





/**
 * /search GET
 * Provides a list of available flights
 * Example request: search/?date=2018-09-02&from=SYD&to=JFK&comingdays=2
 * @param  date         (required)  departure date, YYYY-MM-DD
 * @param  from         (required)  origin airport code, eg: SYD
 * @param  to           (required)  destination airport code, eg: JFK
 * @param  comingdays   (optional)  int +/- comingDays before and after base day
 * @see search()
 */
exports.get = function(req,res){
    if(!req.query.date || !req.query.from || !req.query.to){
        res.json({'error':'Search parameters error','params': ['date','from','to']});
        return;
    }else if(!moment(req.query.date,'YYYY-MM-DD').isValid()){
        res.json({'error':'Date format error', 'param': 'date', 'format': 'YYYY-MM-DD'});
    }
    if(!req.query.comingdays){
        req.query.comingdays = 0;
    }
    search(req.query.date, req.query.from, req.query.to, req.query.comingdays,res);
};
