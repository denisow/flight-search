var Client = require('node-rest-client').Client;
var client = new Client();

var config = require('./config')


/**
 * Flight API client
 * Used to access the Flight API from the current baskend
 */
exports.get = function(query,res,callback){
	var clientReq =  client.get(config.providedApiUrl+query, function (data){
		if(!callback){
			res.json(data);
		}else{
			callback(data);
		}
	});
	clientReq.on('requestTimeout', function (req) {
		if(!callback){
	    	res.json({'error':'request has expired'});
		}
	    clientReq.abort();
	});

	clientReq.on('responseTimeout', function (res) {
		if(!callback){
	    	res.json({'error':'response has expired'});
	    }
	});
	clientReq.on('error', function (err) {
		if(!callback){
			res.json({'error':'request error'});
		}
	});
};
