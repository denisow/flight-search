//List of all flights for all days
var allFlights;


/**
Sort utility
 */
function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

/**
Sort backend data by price
 */
var addFlightsData = function(data){
    data.forEach(function(day){
        day.flights = sortByKey(day.flights,'price');
        allFlights.push(day);
    })
}

/**
Search form validator, enable/disable search button
 */
var formValidation = function(){
    var valid = true;
    $('.flight-form input').each(function(i) {
        if(!$(this).val()){
            valid = false;
        }
    });
    $('.flight-form #searchBtn').prop('disabled', !valid);
}

/**
 Convert airports formats to jQuery autocomplete format
 */
var airportsToAutocompleteValues = function(data){
    var list = [];
    if(data){
        data.forEach(function(airport){
            list.push({label: airport.airportCode+' '+airport.airportName+' '+airport.cityName, value:airport.airportCode});
        });
    }
    return list;
}

/**
 Make a search request on the base day, after receiving a response, make one more for next and previos days
 */
var search = function(){
    allFlights = [];
    var date = $('#date').val();
    var searchParam = { date: date,
                        from:$('#from').val(),
                        to:$('#to').val()};
    showTabContent(date);
    $('a#'+date).addClass('loading');
    $.ajax({
        url: '/search',
        data: searchParam,
        success: function(data) {
            $('#dateTabsContent a').addClass('loading');
            $('a#'+date).removeClass('loading');
            addFlightsData(data);
            showTabContent(date);
            searchParam.comingdays=2;
            $.ajax({
                url: '/search',
                data: searchParam,
                success: function(data) {
                    $('#dateTabsContent a').removeClass('loading');
                    addFlightsData(data);
                    var currentTab = $('#dateTabsContent li.active a')[0].id;
                    if(currentTab != date){
                        showTabContent(currentTab);
                    }
                }
            });
        }
    });
}

/**
Date/Time utilities for template
 */
var dateFormat = function(date,format){
    return moment(date).format(format);
}
var duration = function(min){
    var hours = Math.trunc(min/60);
    var minutes = min % 60;
    return hours+'h '+minutes+'m';
}


/**
Apply allFlights data to flight list template
 */
var showTabContent = function(date){
    $('#dateTabsContent li').removeClass('active');
    $('a#'+date).parent().addClass('active');
    $('.flight-form #flights').empty();
    $('#loadingFlightItems').tmpl({}).appendTo('.flight-form #flights');
    allFlights.forEach(function(day){
        if(day.date == date){
            $('.flight-form #flights').empty();
            if(day.flights && day.flights.length > 0){
                $('#flightItem').tmpl(day).appendTo('.flight-form #flights');
            }else{
                $('#noFlightItems').tmpl({}).appendTo('.flight-form #flights');
            }
        }
    })
}



$(document).ready(function() {
    //Init datepicker
    $('.flight-form #date').datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: moment().add(1, 'h').toDate()
    });

    //Init autocomplete
    $('.flight-form .airport-location').autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: '/airports',
                data: {q: request.term},
                success: function(data) {
                    response(airportsToAutocompleteValues(data));
                }
            });
      },
      minLength: 2,
      select: formValidation
    });
    $('.flight-form input').change(formValidation);
    $('.flight-form input').keypress(formValidation);


    //Init search button and tabs actions
    $('.flight-form #searchBtn').click(function(){
        tmplData = {'tabs': []}
        for (var i = 2; i >= -2; i--) {
            var flightDate = moment($('.flight-form #date').val()).subtract(i, 'day').format('YYYY-MM-DD');
            if(moment(moment(0, 'HH')).diff(flightDate, 'days') <=0 ){
                tmplData.tabs.push({date:flightDate, active:(i==0)});
            }
        }
        $('.flight-form #dateTabsContent').empty();
        $('#dateTab').tmpl(tmplData).appendTo('.flight-form #dateTabsContent');
        $('#dateTabsContent a').click(function(){
            showTabContent(this.id);
        });
        search();
    });
});



