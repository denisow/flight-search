# FlightSearch
JS code task for LOCOMOTE.COM
version 1.0

##Demo
<http://flight.denisow.info/>

##Task
<http://node.locomote.com/code-task/>

##Source
<https://bitbucket.org/denisow/flight-search>



## Install and run

Run script
```
	./start.sh
```
Open <http://localhost:3000/>


##Folder Structure
```
.
├── src                                # Back end
│   ├── config.js                      # Settings
│   ├── index.js                       # Entry point
│   ├── provided-api-service.js        # Client for Flight API
│   └── api
│       ├── airlines.js                # Airlines endpoint
│       ├── airports.js                # Airports endpoint
│       └── search.js                  # Search endpoint
│
├── public                             # Front end
│   ├── index.html                     # Index and templates
│   ├── js
│   │   └── form.js                    # All front end implementation
│   └── ...
│
├── package.json                       # Node package
├── README.md                          # This file
└── start.sh                           # Package installation and launch script
```

son

